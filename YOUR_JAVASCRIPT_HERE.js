//please remove mocha when you attack the enemy, or equip dagger before starting the battle.
const innElement = document.getElementById('inn')
const daggerElement = document.getElementById('dagger')
const bagElement = document.getElementById('bag')
const changeNameForm = document.getElementById('change-name-form')
const attackButton = document.getElementById('attack')
const result = document.getElementById('result')
const heroNameElement = document.getElementById('hero-name')
const heroHealthElement = document.getElementById('hero-health')
const heroWeaponNameElement = document.getElementById('hero-weapon-name')
const heroWeaponDamageElement = document.getElementById('hero-weapon-damage')
const resetButton = document.getElementById('reset')
const enemyImg = document.getElementById('enemy-img')

const healthDefault = 10

const hero = {
  name: 'Mr. Hero',
  heroic: true,
  inventory: [],
  health: healthDefault,
  weapon: {
    type: 'axe',
    damage: 2,
    imgUrl: './img/axe.jpg'
  }
}

const enemy = {
  name: 'Enemy',
  heroic: false,
  inventory: [],
  health: healthDefault,
  weapon: {
    type: 'claw',
    damage: 2,
    imgUrl: './img/claw.jpg'
  }
}

const rest = target => {
  target.health = 10
  return target
}

const pickUpItem = (heroObj, weaponObj) => {
  heroObj.inventory.push(weaponObj)
}

const equipWeapon = heroObj => {
  if (heroObj.inventory.length === 0) return
  heroObj.weapon = heroObj.inventory[0]
}

const displayStatus = hero => {
  heroNameElement.innerHTML = hero.name
  heroHealthElement.innerHTML = hero.health
  heroWeaponNameElement.innerHTML = hero.weapon.type
  heroWeaponDamageElement.innerHTML = hero.weapon.damage
  updateBattleField(hero, enemy)
}

const updateBattleField = (hero, enemy) => {

  const updateHeroEnemyText = targetType => {
    const [heroTarget, enemyTarget] = Array.from(document.querySelectorAll(`.${targetType}`))

    if (targetType === 'weapon') {
      heroTarget.innerHTML = hero[targetType].type
      enemyTarget.innerHTML = enemy[targetType].type
    } else {
      heroTarget.innerHTML = hero[targetType]
      enemyTarget.innerHTML = enemy[targetType]
    }
  }

  ;['name', 'health', 'weapon'].forEach(v=>updateHeroEnemyText(v))
}

const setWeaponImg = () => {
  const arr = [hero, enemy]
  document.querySelectorAll('.weapon-img').forEach((el, i) => {
    //mocha's running test set 'test' to the weapon by default. and assign imgUrl to the dagger object.
    el.src = arr[i].weapon.imgUrl || 'img/dagger.jpg'
  })
}

const showBattleResult = (hero, enemy) => {
  if (hero.health > enemy.health) {
    result.innerHTML = 'You Win!'
  } else if (hero.health < enemy.health) {
    result.innerHTML = 'You Lose...'
  } else {
    result.innerHTML = 'Draw'
  }
}


innElement.addEventListener('click', () => {
  rest(hero)
  displayStatus(hero)
})

daggerElement.addEventListener('click', () => {
  pickUpItem(hero, {
    type: 'dagger',
    damage: 2
  })
})

bagElement.addEventListener('click', () => {
  equipWeapon(hero)
  setWeaponImg()
  displayStatus(hero)
})

changeNameForm.addEventListener('submit', e => {
  e.preventDefault()
  const userInputForm = Array.from(e.target.children).filter(el => el.id.includes('name'))[0]
  hero.name = userInputForm.value
  userInputForm.value = ''

  displayStatus(hero)
})

attackButton.addEventListener('click', () => {
  if (hero.health <= 0 || enemy.health <= 0) return

  const [damageToHero, damageToEnemy] = [enemy.weapon.damage, hero.weapon.damage]

  //aviod health being negative
  hero.health = hero.health < damageToHero ? 0 : hero.health - damageToHero
  enemy.health = enemy.health < damageToEnemy ? 0 : enemy.health - damageToEnemy

  if (hero.health === 0 || enemy.health === 0) {
    showBattleResult(hero, enemy)
  }
  updateBattleField(hero, enemy)
})


resetButton.addEventListener('click', () => {
  hero.health = healthDefault
  enemy.health = healthDefault
  updateBattleField(hero, enemy)

  document.querySelectorAll('.weapon-img').forEach(el => el.style.display = 'block')
  enemyImg.style.opacity = 1
})

enemyImg.addEventListener('click', () => {
  //opacity used in order not to mess layout
  enemyImg.style.opacity = 0
})

const startApp = () =>{
  [document.getElementById('hero-status'), document.getElementById('enemy-status')].forEach(el => {
    const img = document.createElement('img')
    img.classList.add('weapon-img')
    el.appendChild(img)
    el.addEventListener('click', e => {
      e.target.style.display = 'none'
    })
  })
  setWeaponImg()
  updateBattleField(hero, enemy)
  displayStatus(hero)
}

startApp()
